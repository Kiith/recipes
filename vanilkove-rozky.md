Vanillekipferl (Vanilkové rožky)
================================


Dough
-----
1. Mix:
   * 100g fine corn flour (*works with: Natural Jihlava mouka kukuřičná hladká*)
   * 250g fine rice flour (*works with: Natural Jihlava mouka rýžová hladká*)
   * 250g buckwheat flour (*works with: asp pohánková múka*)
   * 110g powdered sugar
   * 290g ground walnuts
   * 70g  vanilla sugar 
     - Can substitute with vanilla beans, vanilla extract
     - Vanillin sugar can be used but will have a worse taste
   * ~1g salt
   * ~8g baking powder
2. Mix in (with hands):
   * 410g butter, solid, cubed
   * **note**: Mix until even, but not too long or the butter will melt
3. Mix in (with hands):
   * 2 whole eggs
   * 2 egg yolks
   * **note**: Mix until even; *the result will be mud-like* - don't expect it to fully solidify
4. Refrigerate overnight in covered bowl


Baking
------
1. Divide small (~250g) pieces one at a time while keeping dough in refrigerator (to stay solid),
2. Roll dough into ~1.5cm thick logs
3. Cut each log into ~7cm pieces, keep pieces until enough to fill baking sheet
4. Don't grease baking sheet or use baking paper 
5. Pre-heat oven to 180C, with top and bottom heat
6. Bend each piece into an 'U' shape and place on baking sheet
7. Place densely (1cm gaps are enough)

   ![placed on a sheet](rozky-raw.jpg)
8. Bake for 20 minutes 

   ![baking](rozky-baking.jpg)


Icing
-----
1. Mix in bowl:
   * 360g powdered sugar
   * 56g vanilla sugar
2. While hot, put 2-3 cookies at a time into bowl and turn until covered in sugar
   * **note**: cookies are extremely easy to break
3. Store cookies


Notes
-----
* [Cheap quality vanilla sugar (SK)](https://bezobalovo.sk/produkt/vanilkovy-cukor/)
* [Natural Jihlava (CZ)](https://www.naturaljihlava.cz/)
* [ASP (SK)](https://www.aspsk.sk/)
