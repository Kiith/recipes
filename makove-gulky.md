Poppy seed balls (Makové guľky)
===============================

Dough
-----
1. Mix:
   * 380g coarse buckwheat flour (*works with: TODO*)
   * 70g  fine rice flour (*works with: Natural Jihlava mouka rýžová hladká*)
   * 150g (gluten-free) oat flour (*works with: Natural Jihlava mouka ovesná bez lepku*)
   * 260g powdered sugar
   * 30g  vanilla sugar
   * 320g ground poppy seed
   * 2g   salt
   * 0.5g cinnamon
2. Mix in (with hands):
   * 450g butter, solid, cubed
   * **note**: Mix until even, but not too long or the butter will melt
3. Mix in (with hands):
   * 2 while eggs
   * 2 egg yolks
   * **note**: Mix until even; *the result will be mud-like* - don't expect it to fully solidify
4. Refrigerate overnight in covered bowl

Baking
------
1. Divide small (~250g) pieces one at a time while keeping dough in refrigerator (to stay solid),
2. Roll dough into ~3.5cm thick logs
3. Don't grease baking sheet or use baking paper 
4. Pre-heat baking oven to 170C, with top and bottom heat (or use fan for multi-level baking)
5. Cut each log into ~3.5cm pieces
6. Form each piece into a ball and place on baking sheet
7. Place densely (2cm gaps are enough)

   ![placed on a sheet](gulky-raw.jpg)
8. Bake for 18 minutes 

   ![baking](gulky-baking-1.jpg)
   ![baking](gulky-baking-2.jpg)

Notes
-----
* [Natural Jihlava (CZ)](https://www.naturaljihlava.cz/)

